/* Dooba SDK
 * Miscellaneous Utilities
 */

// External Includes
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

// Internal Includes
#include "cprintf.h"

// Buffer
uint8_t cprintf_buf[CPRINTF_BUFSIZE];

// Print to String Buffer
uint16_t csnprintf(void *buf, uint16_t len, void *fmt, ...)
{
	uint16_t result;
	va_list ap;

	// Acquire Args
	va_start(ap, fmt);

	// Print
	result = cvsnprintf(buf, len, fmt, ap);

	// Release Args
	va_end(ap);

	return result;
}

// Print to String Buffer - va_list version
uint16_t cvsnprintf(void *buf, uint16_t len, void *fmt, va_list ap)
{
	// Print
	return cvpsnprintf(buf, len, fmt, &ap);
}

// Print to String Buffer - va_list* version
uint16_t cvpsnprintf(void *buf, uint16_t len, void *fmt, va_list *ap)
{
	struct cvsnprintf_printer p;

	// Prep Printer
	p.buf = buf;
	p.len = len;
	p.pos = 0;

	// Print
	cvpprintf((void (*)(void *, uint8_t))cvsnprintf_printer, &p, fmt, ap);

	return p.pos;
}

// String Buffer Printer
void cvsnprintf_printer(struct cvsnprintf_printer *printer, uint8_t c)
{
	// Check Position { Print to String }
	if(printer->pos < printer->len)						{ printer->buf[printer->pos] = c; printer->pos = printer->pos + 1; }
}

// Print Format String to Callback
void cprintf(void (*print)(void *user, uint8_t c), void *user, void *fmt, ...)
{
	va_list ap;

	// Acquire Args
	va_start(ap, fmt);

	// Print
	cvprintf(print, user, fmt, ap);

	// Release Args
	va_end(ap);
}

// Print Format String to Callback - va_list version
void cvprintf(void (*print)(void *user, uint8_t c), void *user, void *fmt, va_list ap)
{
	// Print
	cvpprintf(print, user, fmt, &ap);
}

// Print Format String to Callback - va_list* version
void cvpprintf(void (*print)(void *user, uint8_t c), void *user, void *fmt, va_list *ap)
{
	uint16_t i;
	uint8_t f;
	uint64_t v;
	double vf;
	uint8_t *p;
	uint8_t *s;
	uint8_t x;
	uint8_t vx;
	uint8_t skip;

	// Run through Text
	i = 0;
	p = fmt;
	skip = 0;
	while(p[i] != 0)
	{
		// Look for interpolation
		if((p[i] == '\\') && (p[i + 1] == '%'))			{ skip = 1; }
		else if((p[i] == '%') && (skip == 0))
		{
			// Handle Radix
			i = i + 1;
			f = p[i];
			if(f == 's')								{ cprintf_print(print, user, (uint8_t *)va_arg(*ap, uint8_t *)); }
			else if(f == 't')							{ s = (uint8_t *)va_arg(*ap, uint8_t *); cprintf_print_s(print, user, s, va_arg(*ap, uint16_t)); }
			else if(f == 'c')							{ print(user, va_arg(*ap, int)); }
			else
			{
				// 16-bit types
				if(f == 'h')							{ f = 16; v = va_arg(*ap, uint16_t); }
				else if(f == 'i')						{ f = 10; v = va_arg(*ap, uint16_t); }

				// 32-bit types
				else if(f == 'H')						{ f = 16; v = va_arg(*ap, uint32_t); }
				else if(f == 'I')						{ f = 10; v = va_arg(*ap, uint32_t); }

				// 64-bit types
				else if(f == 'X')						{ f = 16; v = va_arg(*ap, uint64_t); }
				else if(f == 'Y')						{ f = 10; v = va_arg(*ap, uint64_t); }

				// Float
				else if(f == 'f')						{ f = 0; vf = va_arg(*ap, double); dtostrf(vf, 3, 2, (char *)cprintf_buf); s = cprintf_buf; }
				else									{ return; }

				// Convert Value
				if(f)
				{
					x = CPRINTF_BUFSIZE - 1;
					cprintf_buf[x] = 0;
					if(v == 0)							{ x = x - 1; cprintf_buf[x] = '0'; }
					else
					{
						while((v) && (x))
						{
							x = x - 1;
							vx = v % f;
							cprintf_buf[x] = (vx > 9) ? ('a' + (vx - 10)) : ('0' + vx);
							v = v / f;
						}
					}
					s = &(cprintf_buf[x]);
				}

				// Print Value
				cprintf_print(print, user, s);
			}
		}

		// Handle classic text
		else
		{
			// Just print
			skip = 0;
			print(user, p[i]);
		}

		// Loop
		i = i + 1;
	}
}

// Internal String Print Loop
void cprintf_print(void (*print)(void *user, uint8_t c), void *user, void *s)
{
	// Print
	cprintf_print_s(print, user, s, strlen((char *)s));
}

// Internal String Print Loop - Safe
void cprintf_print_s(void (*print)(void *user, uint8_t c), void *user, void *s, uint16_t l)
{
	uint8_t *p = s;
	uint16_t i;

	// Print
	for(i = 0; i < l; i = i + 1)						{ print(user, p[i]); }
}
