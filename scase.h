/* Dooba SDK
 * Miscellaneous Utilities
 */

#ifndef	__UTIL_SCASE_H
#define	__UTIL_SCASE_H

// External Includes
#include <stdint.h>
#include <stdarg.h>

// Upcase Character
extern char upcase_c(char c);

// Downcase Character
extern char downcase_c(char c);

#endif
