/* Dooba SDK
 * Miscellaneous Utilities
 */

#ifndef	__UTIL_HDUMP_H
#define	__UTIL_HDUMP_H

// External Includes
#include <stdint.h>
#include <stdarg.h>

// Hex Dump
extern void hdump(void *x, uint16_t s, uint32_t base_addr, void (*print_cb)(char *fmt, ...));

#endif
