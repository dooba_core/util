/* Dooba SDK
 * Miscellaneous Utilities
 */

#ifndef	__UTIL_CPRINTF_H
#define	__UTIL_CPRINTF_H

// External Includes
#include <stdint.h>
#include <stdarg.h>

// Buffer Size
#define	CPRINTF_BUFSIZE			32

// String Printer Structure
struct cvsnprintf_printer
{
	// Buffer
	uint8_t *buf;

	// Size
	uint16_t len;

	// Position
	uint16_t pos;
};

// Print to String Buffer
extern uint16_t csnprintf(void *buf, uint16_t len, void *fmt, ...);

// Print to String Buffer - va_list version
extern uint16_t cvsnprintf(void *buf, uint16_t len, void *fmt, va_list ap);

// Print to String Buffer - va_list* version
extern uint16_t cvpsnprintf(void *buf, uint16_t len, void *fmt, va_list *ap);

// String Buffer Printer
extern void cvsnprintf_printer(struct cvsnprintf_printer *printer, uint8_t c);

// Print Format String to Callback
extern void cprintf(void (*print)(void *user, uint8_t c), void *user, void *fmt, ...);

// Print Format String to Callback - va_list version
extern void cvprintf(void (*print)(void *user, uint8_t c), void *user, void *fmt, va_list ap);

// Print Format String to Callback - va_list* version
extern void cvpprintf(void (*print)(void *user, uint8_t c), void *user, void *fmt, va_list *ap);

// Internal String Print Loop
extern void cprintf_print(void (*print)(void *user, uint8_t c), void *user, void *s);

// Internal String Print Loop - Safe
extern void cprintf_print_s(void (*print)(void *user, uint8_t c), void *user, void *s, uint16_t l);

#endif
