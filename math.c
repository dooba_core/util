/* Dooba SDK
 * Miscellaneous Utilities
 */

// External Includes
#include <string.h>

// Internal Includes
#include "math.h"

// Clip something to given region (returns 1 if nothing matches)
uint8_t clip_to_region(int *x, int *y, uint16_t *px, uint16_t *py, uint16_t *w, uint16_t *h, uint16_t region_x, uint16_t region_y, uint16_t region_w, uint16_t region_h)
{
	uint16_t region_xe;
	uint16_t region_ye;

	// Compute Region Shortcuts
	region_xe = region_x + region_w;
	region_ye = region_y + region_h;

	// Clip Area
	if((*x) < (int)region_x)				{ if(((int)region_x - (*x)) >= (int)(*w)) { return 1; } else { if(px) { *px = (*px) + ((int)region_x - (*x)); } *w = (*w) - ((int)region_x - (*x)); *x = region_x; } }
	if((*y) < (int)region_y)				{ if(((int)region_y - (*y)) >= (int)(*h)) { return 1; } else { if(py) { *py = (*py) + ((int)region_y - (*y)); } *h = (*h) - ((int)region_y - (*y)); *y = region_y; } }
	if((*x) >= (int)region_xe)				{ return 1; }
	if((*y) >= (int)region_ye)				{ return 1; }
	if(((*x) + (int)(*w)) > (int)region_xe)	{ *w = (int)region_xe - (*x); }
	if(((*y) + (int)(*h)) > (int)region_ye)	{ *h = (int)region_ye - (*y); }

	return 0;
}
