/* Dooba SDK
 * Miscellaneous Utilities
 */

#ifndef	__UTIL_STR_H
#define	__UTIL_STR_H

// External Includes
#include <stdint.h>
#include <stdarg.h>

// Is Number?
#define	isnum(c)							(((c) >= '0') && ((c) <= '9'))

// Is Hex?
#define	ishex(c)							((((c) >= '0') && ((c) <= '9')) || (((c) >= 'a') && ((c) <= 'f')) || (((c) >= 'A') && ((c) <= 'F')))

// Is Alpha?
#define	islca(c)							(((c) >= 'a') && ((c) <= 'z'))
#define	isuca(c)							(((c) >= 'A') && ((c) <= 'Z'))
#define	isalp(c)							(islca(c) || isuca(c))

// Starts With
extern uint8_t str_starts_with(char *s, uint16_t len, char *t);

// Starts With - Fixed Length
extern uint8_t str_starts_with_n(char *s, uint16_t len, char *t, uint16_t t_l);

// Fixed Length Compare
extern uint8_t str_cmp(char *s1, uint16_t s1_len, char *s2, uint16_t s2_len);

// Fixed Length Case-Insensitive Compare
extern uint8_t str_ccmp(char *s1, uint16_t s1_len, char *s2, uint16_t s2_len);

// Fixed Length Find Char
extern uint16_t str_chr(char *s, uint16_t l, char x);

// Fixed Length Reverse Find Char
extern uint16_t str_chr_r(char *s, uint16_t l, char x);

// Find in String
extern int16_t str_find(char *s, uint16_t l, char *t, uint16_t *t_end);

// Find in String - Case
extern int16_t str_find_c(char *s, uint16_t l, char *t, uint16_t *t_end, int16_t ignore_case);

// Multi-Split (Element -> [ Name, Value ])
extern void str_multi_split(char *s, uint16_t off, uint16_t l, uint16_t *pos, char *e_delim, char *v_delim, void (*element_callback)(void *args, uint16_t name, uint16_t name_l, uint16_t value, uint16_t value_l), void *args);

// Check Command and Extract Arguments
extern uint8_t str_is_cmd(char *s, uint16_t l, char *cmd, char **args, uint16_t *args_len);

// Get Next Argument from String
extern uint8_t str_next_arg(char **s, uint16_t *l, char **arg, uint16_t *arg_len);

// Peek Next Argument from String
extern uint8_t str_peek_next_arg(char **s, uint16_t *l, char **arg, uint16_t *arg_len);

// Unescape String
extern uint16_t str_unesc(char *s);

// Unescape String - Fixed Length
extern uint16_t str_unesc_n(char *s, uint16_t l, uint8_t zeroterminate);

#endif
