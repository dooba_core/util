/* Dooba SDK
 * Miscellaneous Utilities
 */

#ifndef	__UTIL_ASSERT_H
#define	__UTIL_ASSERT_H

// External Includes
#include <stdint.h>
#include <assert.h>

// Define static_assert if not defined
#ifndef	static_assert
#define	static_assert(expr, msg)					_Static_assert(expr, msg)
#endif

#endif
