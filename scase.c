/* Dooba SDK
 * Miscellaneous Utilities
 */

// External Includes
#include <stdint.h>
#include <string.h>

// Internal Includes
#include "scase.h"

// Upcase Character
char upcase_c(char c)
{
	// Upcase a-z
	if((c >= 'a') && (c <= 'z'))						{ return c - 0x20; }

	return c;
}

// Downcase Character
char downcase_c(char c)
{
	// Upcase A-Z
	if((c >= 'A') && (c <= 'Z'))						{ return c + 0x20; }

	return c;
}
