/* Dooba SDK
 * Miscellaneous Utilities
 */

// External Includes
#include <string.h>

// Internal Includes
#include "hdump.h"

// Hex Dump
void hdump(void *x, uint16_t s, uint32_t base_addr, void (*print_cb)(char *fmt, ...))
{
	uint16_t i;
	uint8_t j;
	uint8_t *p;
	uint32_t o;

	// Run
	i = 0;
	p = x;
	while(i < s)
	{
		o = base_addr + i;
		print_cb("%h%h%h%h%h%h%h%h | ", (uint16_t)((o >> 28) & 0x0f), (uint16_t)((o >> 24) & 0x0f), (uint16_t)((o >> 20) & 0x0f), (uint16_t)((o >> 16) & 0x0f), (uint16_t)((o >> 12) & 0x0f), (uint16_t)((o >> 8) & 0x0f), (uint16_t)((o >> 4) & 0x0f), (uint16_t)(o & 0x0f));
		for(j = 0; j < 16; j = j + 1)					{ if((i + j) < s) { print_cb("%h%h ", (p[i + j] >> 4) & 0x0f, p[i + j] & 0x0f); } else { print_cb("   "); } }
		print_cb("| ");
		for(j = 0; j < 16; j = j + 1)					{ if((i + j) < s) { print_cb("%c ", (((p[i + j] >= ' ') && (p[i + j] <= '~')) ? p[i + j] : '.')); } else { print_cb("  "); } }
		print_cb("\n");
		i = i + 16;
	}
}
