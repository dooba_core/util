/* Dooba SDK
 * Miscellaneous Utilities
 */

// External Includes
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

// Internal Includes
#include "cprintf.h"
#include "str.h"

// Starts With
uint8_t str_starts_with(char *s, uint16_t len, char *t)
{
	// Starts With
	return str_starts_with_n(s, len, t, strlen(t));
}

// Starts With - Fixed Length
uint8_t str_starts_with_n(char *s, uint16_t len, char *t, uint16_t t_l)
{
	// Check Lengths
	if(len < t_l)																								{ return 0; }

	// Compare
	return !(memcmp(s, t, t_l));
}

// Fixed Length Compare
uint8_t str_cmp(char *s1, uint16_t s1_len, char *s2, uint16_t s2_len)
{
	// Check Lengths
	if(s1_len != s2_len)																						{ return 1; }

	// Check Strings
	return memcmp(s1, s2, s1_len);
}

// Fixed Length Case-Insensitive Compare
uint8_t str_ccmp(char *s1, uint16_t s1_len, char *s2, uint16_t s2_len)
{
	// Check Lengths
	if(s1_len != s2_len)																						{ return 1; }

	// Check Strings
	return strncasecmp(s1, s2, s1_len);
}

// Fixed Length Find Char
uint16_t str_chr(char *s, uint16_t l, char x)
{
	uint16_t i;

	// Find
	for(i = 0; i < l; i = i + 1)																				{ if(s[i] == x) { return i; } }

	return l;
}

// Fixed Length Reverse Find Char
uint16_t str_chr_r(char *s, uint16_t l, char x)
{
	uint16_t i;

	// Find
	for(i = l - 1; i > 0; i = i - 1)																			{ if(s[i] == x) { return i; } }
	return (s[0] == x) ? 0 : l;
}

// Find in String
int16_t str_find(char *s, uint16_t l, char *t, uint16_t *t_end)
{
	// Find
	return str_find_c(s, l, t, t_end, 0);
}

// Find in String - Case
int16_t str_find_c(char *s, uint16_t l, char *t, uint16_t *t_end, int16_t ignore_case)
{
	int16_t result = -1;
	int16_t tl = 0;
	int16_t sl = 0;
	int16_t i = 0;
	int16_t (*cmp)(char *, char *, size_t) = 0;

	// Check String & Token
	if((!s) || (l <= 0) || (!t))																				{ return -1; }

	// Acquire Token Length
	tl = strlen(t);

	// Check Token
	if(tl <= 0)																									{ return 0; }

	// Find
	sl = l - tl;
	cmp = (int16_t (*)(char *, char *, size_t))(ignore_case ? strncasecmp : strncmp);
	for(i = 0; (i <= sl) && (result < 0); i = i + 1)															{ if(cmp(&(s[i]), t, tl) == 0) { result = i; } }

	// Set Token End
	if((result >= 0) && (t_end))																				{ *t_end = result + tl; }

	return result;
}

// Multi-Split (Element -> [ Name, Value ])
void str_multi_split(char *s, uint16_t off, uint16_t l, uint16_t *pos, char *e_delim, char *v_delim, void (*element_callback)(void *args, uint16_t name, uint16_t name_l, uint16_t value, uint16_t value_l), void *args)
{
	int16_t rest_l = 0;
	int16_t sl = 0;
	uint16_t t_end = 0;
	uint16_t p = 0;
	int16_t e_sl = 0;
	uint16_t e_t_end = 0;
	int16_t done = 0;

	// Check String, Delims & Callback
	if((!s) || (l <= 0) || (!e_delim) || (!v_delim) || (!element_callback))										{ return; }

	// Start
	p = off;
	rest_l = l;

	// Loop
	while(!done)
	{
		// Acquire Element
		sl = str_find(&(s[p]), rest_l, e_delim, &t_end);
		if(sl < 0)
		{
			// Last Element
			sl = rest_l;
			t_end = rest_l;
			done = 1;
		}

		// Split Element
		e_sl = str_find(&(s[p]), sl, v_delim, &e_t_end);
		if(e_sl >= 0)																							{ element_callback(args, p, e_sl, p + e_t_end, sl - e_t_end); }

		// NEXT
		p = p + t_end;
		rest_l = rest_l - t_end;
		if(rest_l <= 0)																							{ done = 1; }
	}

	// Set Position
	if(pos)																										{ *pos = p; }
}

// Check Command and Extract Arguments
uint8_t str_is_cmd(char *s, uint16_t l, char *cmd, char **args, uint16_t *args_len)
{
	uint16_t cmd_l;
	uint16_t s_l;

	// Get Lengths
	cmd_l = strlen(cmd);
	s_l = 0;
	while((s_l < l) && (s[s_l] != ' '))																			{ s_l = s_l + 1; }

	// Check Command
	if(s_l != cmd_l)																							{ return 0; }
	if(memcmp(s, cmd, s_l))																						{ return 0; }

	// Command Match - Extract Arguments
	if(args)																									{ *args = &(s[s_l + 1]); }
	if(args_len)																								{ *args_len = ((s_l + 1) < l) ? (l - (s_l + 1)) : 0; }

	return 1;
}

// Get Next Argument from String
uint8_t str_next_arg(char **s, uint16_t *l, char **arg, uint16_t *arg_len)
{
	char *ss;
	uint16_t ll;
	uint16_t x;
	uint16_t off;
	uint8_t q;
	uint8_t done;

	// Check Length
	if(*l == 0)																									{ return 0; }

	// Init
	ss = *s;
	ll = *l;
	x = 0;
	q = 0;

	// Detect Quotes
	if((ss[0] == '"') || (ss[0] == '\''))																		{ q = ss[0]; x = x + 1; }

	// Run through String
	done = 0;
	while((x < ll) && (!done))
	{
		// Handle Quoted Arg
		if(q)																									{ if(ss[x] == q) { done = 1; } else { x = x + 1; } }

		// Handle Normal Arg
		else																									{ if(ss[x] == ' ') { done = 1; } else { x = x + 1; } }
	}

	// Set Arg
	if(arg)																										{ *arg = &(ss[q ? 1 : 0]); }
	if(arg_len)																									{ *arg_len = x - (q ? 1 : 0); }

	// Next
	off = x + (q ? 2 : 1);
	*s = &(ss[off]);
	*l = (off < ll) ? (ll - off) : 0;

	return 1;
}

// Peek Next Argument from String
uint8_t str_peek_next_arg(char **s, uint16_t *l, char **arg, uint16_t *arg_len)
{
	char *ps;
	uint16_t pl;
	uint8_t r;

	// Save previous
	ps = *s;
	pl = *l;

	// Get next
	r = str_next_arg(s, l, arg, arg_len);

	// Restore previous
	*s = ps;
	*l = pl;

	return r;
}

// Unescape String
uint16_t str_unesc(char *s)
{
	// Unescape
	return str_unesc_n(s, strlen(s), 1);
}

// Unescape String - Fixed Length
uint16_t str_unesc_n(char *s, uint16_t l, uint8_t zeroterminate)
{
	uint16_t i;
	uint16_t j;
	uint16_t clen;
	char b[5];
	char x;

	// Run through String
	i = 0;
	while(i < l)
	{
		// Find Escape Sequences
		if((s[i] == '\\') && ((i + 1) < l))
		{
			// Newline
			if(s[i + 1] == 'r')																					{ x = '\r'; clen = 1; }
			else if(s[i + 1] == 'n')																			{ x = '\n'; clen = 1; }
			else if(s[i + 1] == '\\')																			{ x = '\\'; clen = 1; }
			else if((s[i + 1] == 'x') && ((i + 3) < l) && (ishex(s[i + 2])) && (ishex(s[i + 3])))				{ j = csnprintf(b, 5, "0%t", &(s[i + 1]), 3); b[j] = 0; x = strtoul(b, 0, 0); clen = 3; }
			else																								{ clen = 0; x = 0; }

			// Check Escape Sequence
			if(clen)
			{
				// Replace with byte
				s[i] = x;
				j = i + clen + 1;
				while(j < l)																					{ s[j - clen] = s[j]; j = j + 1; }
				l = l - clen;
			}
		}

		// Next
		i = i + 1;
	}

	// Zero-terminate
	if(zeroterminate)																							{ s[l] = 0; }

	return l;
}
