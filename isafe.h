/* Dooba SDK
 * Miscellaneous Utilities
 */

#ifndef	__UTIL_ISAFE_H
#define	__UTIL_ISAFE_H

// External Includes
#include <stdint.h>

// Minimal Delay
#define	isafe_delay()						asm volatile ("lpm" "\n" "lpm" "\n" "lpm" "\n" "nop" "\n");

// Interrupt-Safe Read Short Value
extern uint16_t isafe_rds(volatile uint16_t *v);

#endif
