/* Dooba SDK
 * Miscellaneous Utilities
 */

#ifndef	__UTIL_BSWAP_H
#define	__UTIL_BSWAP_H

// External Includes
#include <stdint.h>

// Byte Swap Short
#define	bswap_s(x)								(((x >> 8) & 0x00ff) | ((x << 8) & 0xff00))

// Byte Swap Long
#define	bswap_l(x)								(((x >> 24) & 0x000000ff) | ((x >> 8) & 0x0000ff00) | ((x << 8) & 0x00ff0000) | ((x << 24) & 0xff000000))

#endif
