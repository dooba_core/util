/* Dooba SDK
 * Miscellaneous Utilities
 */

#ifndef	__UTIL_MATH_H
#define	__UTIL_MATH_H

// External Includes
#include <stdint.h>
#include <stdarg.h>

// Dirty Definition of PI
#define	PI										3.1415

// Clip something to given region (returns 1 if nothing matches)
extern uint8_t clip_to_region(int *x, int *y, uint16_t *px, uint16_t *py, uint16_t *w, uint16_t *h, uint16_t region_x, uint16_t region_y, uint16_t region_w, uint16_t region_h);

#endif
