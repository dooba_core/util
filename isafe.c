/* Dooba SDK
 * Miscellaneous Utilities
 */

// External Includes
#include <string.h>

// Internal Includes
#include "isafe.h"

// Interrupt-Safe Read Short Value
uint16_t isafe_rds(volatile uint16_t *v)
{
	uint16_t result;
	uint16_t r;

	// Loop
	r = 0;
	result = 1;
	while(r != result)
	{
		// Read
		result = *v;

		// Delay
		isafe_delay();

		// Re-Read
		r = *v;
	}

	return result;
}
